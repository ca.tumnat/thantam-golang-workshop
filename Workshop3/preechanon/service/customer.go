package service

import "hexa_arh/repository"

type CustomerResponse struct {
	CustomerId   int    `json:"customerid"`
	CustomerName string `json:"customername"`
	Phone_number string `json:"phone_number"`
	Date_created string `json:"date_created"`
}

type CustomerService interface {
	GETCustomers() ([]CustomerResponse, error)
	GETCustomer(int) (*CustomerResponse, error)
	UPDATECustomer(repository.Customer, int) (int, error)
	ADDCustomer(repository.Customer) (int, error)
	DELETECustomer(int) error
}
