package repository

type customerRepositoryMock struct {
	customers []Customer
}

func NewCustomerRepositoryMock() customerRepositoryMock {
	customers := []Customer{
		{CustomerId: 1, CustomerName: "John", Phone_number: "0777777771", Date_created: "04/24/2023"},
		{CustomerId: 2, CustomerName: "Gakpo", Phone_number: "0899874561", Date_created: "04/24/2023"},
	}

	return customerRepositoryMock{customers: customers}
}

//func (m customerRepositoryMock) GetCustomerById(customerId int) (Customer, error) {
