package repository

type Product struct {
	ID   int     `json: "id"`
	Product_Name string  `json: "name"`
	Price      float64 `json: "price"`
	Product_Detail   string  `json: "detail"`
	Date_Created string `json: "date"`
}

type ProductRepository interface {
	GetAll() ([]Product, error)
	GetById(int) (*Product, error)
	GetPost(Product)(int, error)
	GetDeleteById(int) error
	GetUpdate(Product)(int ,error)
	

}