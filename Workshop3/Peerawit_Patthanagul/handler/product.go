package handler

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	//"github.com/Peerawitptk/go4web/errs"
	//"github.com/Peerawitptk/go4web/repository"
	"github.com/Peerawitptk/go4web/service"
	"github.com/gorilla/mux"
)

type productHandler struct {
	productsrv service.ProductService
}

func NewProductHandler(productsrv service.ProductService)productHandler{
	return productHandler{productsrv: productsrv}
}

func (h productHandler) GetProducts(w http.ResponseWriter, r *http.Request){
	products, err := h.productsrv.GetProducts()
	if err != nil{
		handlerError(w,err)
		return
	}
	w.Header().Set("Content-Type","application/json")
	json.NewEncoder(w).Encode(products)

}

func (h productHandler) GetProduct(w http.ResponseWriter, r *http.Request){
	id,_ := strconv.Atoi(mux.Vars(r)["id"])

	product, err := h.productsrv.GetProduct(id)
	if err != nil{
		handlerError(w, err)
		return 
	}
	w.Header().Set("content-type","application/json")
	json.NewEncoder(w).Encode(product)
} 

func (h productHandler) GetPost(w http.ResponseWriter, r *http.Request){
	var product service.ProductResponse
	err := json.NewDecoder(r.Body).Decode(&product)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	insertedID, err := h.productsrv.GetPost(product)  

	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(fmt.Sprintf(`{"Customerid:%d"}`, insertedID)))
}

func (h productHandler) GetDeleteById(w http.ResponseWriter, r *http.Request){
	ID, _ := strconv.Atoi(mux.Vars(r)["id"])
	err := h.productsrv.GetDeleteById(ID)
	if err != nil{
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Println(w, err)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Customer successfully removed"))
}


func (h productHandler) GetUpdate(w http.ResponseWriter, r *http.Request){
	var product service.ProductResponse
		err := json.NewDecoder(r.Body).Decode(&product)
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		updateID, err := h.productsrv.GetUpdate(product)   

		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(fmt.Sprintf(`{"Customerid:%d"}`, updateID)))
}

