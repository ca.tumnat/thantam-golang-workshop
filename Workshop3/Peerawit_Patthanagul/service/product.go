package service

//import "github.com/Peerawitptk/go4web/repository"

type ProductResponse struct {
	ID             int     `json: "id"`
	Product_Name   string  `json: "name"`
	Price          float64 `json: "price"`
	Product_Detail string  `json: "detail"`
	Date_Created   string  `json: "date"`
}



type ProductService interface {
	GetProducts() ([]ProductResponse, error)
	GetProduct(int) (*ProductResponse, error)
	GetPost(ProductResponse)(int, error)
	GetDeleteById(int) error
	GetUpdate(ProductResponse)(int ,error)
}
