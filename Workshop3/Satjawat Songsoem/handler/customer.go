package handler

import (
	"bank/errs"
	"bank/service"
	"encoding/json"
	"fmt"

	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type customerHandler struct {
	custSrv service.CustomerService
}

func NewCustomerHandler(custSrv service.CustomerService) customerHandler{
	return customerHandler{custSrv: custSrv}
}

func (h customerHandler) GetCustomers(w http.ResponseWriter, r *http.Request) {
	customers, err := h.custSrv.GetCustomers()
	if err != nil{
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Println(w, err)
		return
	}
	json.NewEncoder(w).Encode(customers)

}

func (h customerHandler) RemoveCustomer(w http.ResponseWriter, r *http.Request){
	customerID, _ := strconv.Atoi(mux.Vars(r)["customerID"])
	err := h.custSrv.RemoveCustomer(customerID)
	if err != nil{
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Println(w, err)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Customer successfully removed"))
}


func (h customerHandler) InsertCustomer(w http.ResponseWriter, r *http.Request){
		var customer service.CustomerResponse
		err := json.NewDecoder(r.Body).Decode(&customer)
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		insertedID, err := h.custSrv.InsertCustomer(customer)   

		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(fmt.Sprintf(`{"Customerid:%d"}`, insertedID)))
}

func (h customerHandler) UpdateCustomer(w http.ResponseWriter, r *http.Request){
	var customer service.CustomerResponse
		err := json.NewDecoder(r.Body).Decode(&customer)
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		updateID, err := h.custSrv.UpdateCustomer(customer)   

		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(fmt.Sprintf(`{"Customerid:%d"}`, updateID)))
}

func (h customerHandler) GetCustomer(w http.ResponseWriter, r *http.Request){
	customerID, _ := strconv.Atoi(mux.Vars(r)["customerID"])
	customer, err :=  h.custSrv.GetCustomer(customerID)
	if err != nil{

		appErr, ok := err.(errs.AppError)
		if ok {
			w.WriteHeader(appErr.Code)
			fmt.Println(w, appErr.Message)
			return
		}
		
	}
	json.NewEncoder(w).Encode(customer)
}