package handler

import (
	"bank/errs"
	"bank/service"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type productHandler struct {
	productService service.ProductService
}

func NewProductHandler(productService service.ProductService) productHandler {
	return productHandler{productService: productService}

}

func (h productHandler) AddProduct(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("content-type") != "application/json" {
		handlerError(w, errs.NewValidationError("request body incorrect format"))
		return
	}

	var request service.ProductResponse
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		handlerError(w, errs.NewValidationError("request body incorrect format"))
		return
	}

	productID, err := h.productService.AddProduct(request)
	if err != nil {
		handlerError(w, err)
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(map[string]int{"product_id": productID})
}

func (h productHandler) GetProducts(w http.ResponseWriter, r *http.Request) {
	product, err := h.productService.GetProducts()
	if err != nil {
		handlerError(w, err)
		return
	}
	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(product)
}

func (h productHandler) GetProduct(w http.ResponseWriter, r *http.Request) {
	productID, _ := strconv.Atoi(mux.Vars(r)["productID"])

	product, err := h.productService.GetProduct(productID)
	if err != nil {
		handlerError(w, err)
		return
	}
	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(product)
}

func (h productHandler) UpdateProduct(w http.ResponseWriter, r *http.Request) {
	productId, _ := strconv.Atoi(mux.Vars(r)["productID"])

	if r.Header.Get("content-type") != "application/json" {
		handlerError(w, errs.NewValidationError("request body incorrect format"))
		return
	}

	var request service.ProductResponse
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		handlerError(w, errs.NewValidationError("request body incorrect format"))
		return
	}

	err = h.productService.UpdateProduct(productId, request)
	if err != nil {
		handlerError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (h productHandler) DeleteProduct(w http.ResponseWriter, r *http.Request) {
	productID, _ := strconv.Atoi(mux.Vars(r)["productID"])
	err := h.productService.DeleteProduct(productID)
	if err != nil {
		handlerError(w, err)
		return
	}
	w.WriteHeader(http.StatusOK)
}
