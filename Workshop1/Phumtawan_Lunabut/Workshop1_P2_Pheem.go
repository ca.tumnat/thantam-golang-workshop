package main

import "fmt"

func main() {
	var current, previous int = 1, 0
	const count = 10

	for i := 0; i < count; i++ {
		fmt.Printf("%d ", previous)
		current, previous = current+previous, current
	}
}
