package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var reader = bufio.NewReader(os.Stdin)

func getInput(promt string)float64 {
	fmt.Printf("%v", promt)
	input, _ := reader.ReadString('\n')
	value, err := strconv.ParseFloat(strings.TrimSpace(input), 64)
	if err != nil {
		message, _ := fmt.Scanf("%v must number only", promt)
		panic(message)
	}
	return value
}
func add(value1, value2 float64) float64{
	return value1+value2
}
func minus(value1, value2 float64) float64{
	return value1 - value2
}
func multiply(value1, value2 float64) float64{
	return value1 * value2
}
func divide(value1, value2 float64) float64{
	if value2 == 0{
		return 0
	}else{
		return value1 / value2
	}
}

func main() {
	value1 := getInput("Enter first number: \n")
	value2 := getInput("Enter second number: \n")
	if value2 == 0{
		panic("Please enter second number not zero")
	}
	
	result := add(value1, value2)
	result1 := minus(value1, value2)
	result2 := multiply(value1, value2)
	result3 := divide(value1, value2)

	fmt.Println("Addition result ",result)
	fmt.Println("Subtraction result: ",result1)
	fmt.Println("Multiplication result: ",result2)
	fmt.Println("Division result: ",result3)
}
