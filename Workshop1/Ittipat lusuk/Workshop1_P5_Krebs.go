package main

import "fmt"

func gcd(a, b int) int {
	for b != 0 {
		a, b = b, a%b
	}
	return a
}

func main() {
	list := []int{2, 4, 6, 8, 10}
	result := list[0]
	for i := 1; i < len(list); i++ {
		result = result * list[i] / gcd(result, list[i])
	}
	fmt.Println("LCM this list", list, "is", result)
}
