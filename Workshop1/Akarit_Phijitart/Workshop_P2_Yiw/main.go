package main

import "fmt"

func main() {
	// Initialize the first two Fibonacci numbers.
	a := 0
	b := 1
	// Generate the next 10 Fibonacci numbers and print them out.
	for i := 0; i < 10; i++ {
		fmt.Printf("%d ", a)

		// Compute the next Fibonacci number and update a and b.
		a, b = b, a+b
	}
	fmt.Println()
	// fib(20)
}
