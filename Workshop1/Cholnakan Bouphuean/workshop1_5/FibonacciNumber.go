package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func getAmount(prompt string) int {
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Print(prompt)
		input, err := reader.ReadString('\n')
		if err != nil {
			panic(err)
		}
		input = strings.TrimSpace(input)
		amount, err := strconv.Atoi(input)
		if err != nil {
			fmt.Printf("%v must be a number\n", prompt)
			continue
		}
		return amount
	}
}

func fib(n int) int {
	var fibNum int
	if n <= 1 {
		fibNum = n
	} else {
		fibNum = fib(n-1) + fib(n-2)
	}
	return fibNum
}

func fibList(n int) []int {
	var fibList []int
	for i := 0; i < n; i++ {
		fibList = append(fibList, fib(i))
	}

	return fibList
}

func main() {
	n := getAmount("Amount of FibonacciNumber : ")
	fib := fibList(n)
	fmt.Printf("Fibonacci Number (%[1]v) : %[2]v", n, fib)
}
