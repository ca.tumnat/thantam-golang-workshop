package main

import "fmt"

type productList struct {
	productName   string
	priceProduct  int
	weightProduct float32
}

func main() {
	products := []productList{}
	productList1 := productList{
		productName:   "iPhone 13",
		priceProduct:  26900,
		weightProduct: 0.21,
	}
	products = append(products, productList1)

	fmt.Println("Product name : ", productList1.productName)
	fmt.Println("Price : ", productList1.priceProduct)
	fmt.Println("Weight: ", productList1.weightProduct, "kg")
}
