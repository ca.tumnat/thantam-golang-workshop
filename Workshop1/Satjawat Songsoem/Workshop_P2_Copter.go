package main

import (
	"fmt"
)

var c int

func primeNumberCheck(number int) {

	for i := 2; i < number; i++ {
		if number%i == 0 {
			c += 1
		}
	}

	if c >= 1 {
		fmt.Println("Not Prime")
	} else {
		fmt.Println("Prime")
	}
}

func main() {
	primeNumberCheck(17)
}
