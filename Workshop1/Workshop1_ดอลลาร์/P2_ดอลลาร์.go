package main

import "fmt"

func isPrime(number int) string {
	if number == 0 || number == 1 {
		return "false"
	}
	for i := 2; i < number; i++ {
		if number%i == 0 {
			return "false"
		}
	}
	return "true"
}
func main() {
	result := isPrime(17)
	fmt.Println(result)
}
