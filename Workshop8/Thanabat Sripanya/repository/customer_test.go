package repository_test

import (
	"server/repository"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

// Define a mock struct for the GORM database interface
type mockDB struct {
	mock.Mock
}

// Implement the GORM database interface for the mock struct
func (m *mockDB) Create(customer *repository.Customer) error {
	args := m.Called(customer)
	return args.Error(0)
}

// Define a test for creating a new customer
func TestCreateCustomer(t *testing.T) {
	// Create a new mock database instance
	db := new(mockDB)

	// Set up expectations for the Create method on the mock database
	db.On("Create", mock.AnythingOfType("*repository.Customer")).Return(nil)

	// Create a new customer instance to be saved to the database
	customer := &repository.Customer{
		CustomerName: "John Doe",
		PhoneNumber:  "555-1234",
		BirthDate:    time.Now().Format("2006-01-02"),
	}

	// Save the customer to the mock database
	err := db.Create(customer)

	// Assert that the Create method was called on the mock database with the correct arguments
	db.AssertCalled(t, "Create", customer)

	// Assert that the save operation was successful
	assert.NoError(t, err)
}
