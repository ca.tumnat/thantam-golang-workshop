package main

import (
	
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

var Db *sql.DB
const coursePath = "products"
const basePath = "/api"


func SetupDB(){
	var err error
	Db, err = sql.Open("mysql","root:pee1234@tcp(127.0.0.1:3306)/coursedata")

	if err != nil{
		log.Fatal(err)
	}
	fmt.Println(Db)
	Db.SetConnMaxIdleTime(time.Minute * 3)
	Db.SetMaxOpenConns(10)
	Db.SetMaxIdleConns(10)
}

type Product struct {
	ID   int     `json: "id"`
	Product_Name string  `json: "name"`
	Price      float64 `json: "price"`
	Product_Detail   string  `json: "detail"`
	Date_Created string `json: "date"`
}

func handleProducts(w http.ResponseWriter, r *http.Request) {
	switch r.Method{
	case http.MethodGet:
		productList, err := getProductList()
		if err != nil{
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		j, err := json.Marshal(productList)
		if err != nil{
			log.Fatal(err)
		}
		_, err = w.Write(j)
		if err != nil {
			log.Fatal(err)
		}
	case http.MethodPost:
		var product Product
		err := json.NewDecoder(r.Body).Decode(&product)
		if err != nil{
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		ID, err := insertProduct(product)
		if err != nil{
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(fmt.Sprintf(`{"id":%d}`, ID)))
	case http.MethodOptions:
		return
	case http.MethodPut:
		var product Product
		err := json.NewDecoder(r.Body).Decode(&product)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		ID, err := putProduct(product)

		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(fmt.Sprintf(`{"id:%d"}`, ID)))

	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func handleProduct(w http.ResponseWriter, r *http.Request) {
	urlPathSegment := strings.Split(r.URL.Path, fmt.Sprintf("%s/", coursePath))
	if len(urlPathSegment[1:]) > 1 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	ID, err := strconv.Atoi(urlPathSegment[len(urlPathSegment)-1])
	if err != nil{
		log.Print(err)
		w.WriteHeader(http.StatusNotFound)
		return
	}
	switch r.Method{
	case http.MethodGet:
		product, err := getProduct(ID)
		if err != nil{
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		if product == nil{
			w.WriteHeader(http.StatusNotFound)
			return
		}
		j, err := json.Marshal(product)
		if err != nil{
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		_, err = w.Write(j)
		if err != nil {
			log.Fatal(err)
		}
	case http.MethodDelete:
		err := removeProduct(ID)
		if err != nil{
			log.Print(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func getProductList() ([]Product, error){
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	results, err := Db.QueryContext(ctx, `SELECT
	id,
	product_name,
	price,
	product_detail,
	date_created
	FROM product`)
	if err != nil{
		log.Println(err.Error())
		return nil, err
	}
	defer results.Close()
	products := make([]Product, 0)
	for results.Next(){
		var product Product
		results.Scan(&product.ID,
		&product.Product_Name,
		&product.Price,
		&product.Product_Detail,
		&product.Date_Created)

		products = append(products, product)
	}
	return products, nil
}

func insertProduct(product Product)(int, error){
	ctx, cancel := context.WithTimeout(context.Background(),3*time.Second)
	defer cancel()
	result, err := Db.ExecContext(ctx, `INSERT INTO product(
		id,
		product_name,
		price,
		product_detail,
		date_created
	) VALUES (?,?,?,?,?)`,
		product.ID,
		product.Product_Name,
		product.Price,
		product.Product_Detail,
		time.Now())
	if err != nil{
		log.Println(err.Error())
		return 0, err
	}else {
		log.Printf("Insert Product :", product)
	}

	insertID, err := result.LastInsertId()
	if err != nil{
		log.Println(err.Error())
		return 0, err
	}
	return int(insertID), nil
}

func getProduct(id int)(*Product, error){
	ctx, cancle := context.WithTimeout(context.Background(),3*time.Second)
	defer cancle()
	row := Db.QueryRowContext(ctx,`SELECT
	id,
	product_name,
	price,
	product_detail,
	date_created
	FROM product
	WHERE id = ?`, id)

	product := &Product{}
	err := row.Scan(
		&product.ID,
		&product.Product_Name,
		&product.Price,
		&product.Product_Detail,
		&product.Date_Created,
	)
	if err == sql.ErrNoRows{
		return nil, nil
	}else if err != nil{
		log.Println(err)
		return nil, err
	}
	return product, nil
}

func putProduct(product Product) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	result, err := Db.ExecContext(ctx, `UPDATE product SET product_name = ?, price = ?, product_detail = ? WHERE id = ?`,
		product.Product_Name, product.Price, product.Product_Detail, product.ID)
	if err != nil {
		log.Println(err.Error())
		return 0, err
	} else {
		log.Printf("Update :", product)
	}
	updateID, err := result.LastInsertId()
	if err != nil {
		log.Println(err.Error())
		return 0, err
	}
	return int(updateID), nil
}



func removeProduct(id int)error{
	ctx, cancle := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancle()
	_, err := Db.ExecContext(ctx, `DELETE FROM product where id = ?`, id)
	if err != nil{
		log.Println(err.Error())
		return err
	}else{
		log.Printf("Remove ID :%d ", id)
	}
	return nil
}


func corsMiddleware(handler http.Handler) http.Handler{
	return http.HandlerFunc(func(w http.ResponseWriter, r*http.Request){
		w.Header().Add("Access-Control-Allow-Origin","*")
		w.Header().Add("Content-Type","applicaiton/json")
		w.Header().Set("Access-Control-Allow-Methods","POST, GET, OPTIONS, DELETE, PUT")
		w.Header().Set("Access-Control-Allow-Headers","Accept, Content-Type, Content-Length, Authorization")
		handler.ServeHTTP(w, r)
	})
}

func SetupRoutes(apiBasePath string){
	productHandler := http.HandlerFunc(handleProduct)
	http.Handle(fmt.Sprintf("%s/%s/", apiBasePath, coursePath), corsMiddleware(productHandler))
	productsHandler := http.HandlerFunc(handleProducts)
	http.Handle(fmt.Sprintf("%s/%s", apiBasePath, coursePath), corsMiddleware(productsHandler))
	
}

func main() {

	SetupDB()
	SetupRoutes(basePath)
	log.Fatal(http.ListenAndServe(":5000", nil))
}