package repository

import (
	"log"

	"gorm.io/gorm"
)

type productRepositoryDB struct {
	db *gorm.DB
}

func NewProductRepositoryDB(db *gorm.DB) productRepositoryDB {
	return productRepositoryDB{db: db}
}

func (r productRepositoryDB) GetAll() ([]Product, error) {
	var product []Product

	// Use GORM to retrieve all product records from the database
	tx := r.db.Find(&product)

	// Check for errors
	if tx.Error != nil {
		log.Println(tx.Error)
		return nil, tx.Error
	}

	return product, nil
}

func (r productRepositoryDB) GetById(id int) (*Product, error) {

	// query := "SELECT id, product_name, price, product_detail, date_created FROM tcc_workshop.product WHERE id = ?"

	products := Product{}
	tx := r.db.First(&products, id)
	if tx.Error != nil {
		log.Println(tx.Error)
		return nil, tx.Error
	}
	return &products, nil
}

func (r productRepositoryDB) Create(product Product) (*Product, error) {
	// query := "INSERT INTO `tcc_workshop`.`product` (`product_name`, `price`, `product_detail`, `date_created`) VALUES (?, ?, ?, ?);"
	tx := r.db.Create(&product)
	if tx.Error != nil {
		log.Println(tx.Error)
		return nil, tx.Error
	}

	return &product, nil
}

func (r productRepositoryDB) Update(product Product) (*Product, error) {
	// Get the ID of the product to update
	id := product.ID

	// Use GORM to update the product record
	tx := r.db.Model(&Product{}).Where("id = ?", id).Updates(Product{
		ProductName:   product.ProductName,
		Price:         product.Price,
		ProductDetail: product.ProductDetail,
		DateCreated:   product.DateCreated,
	})

	// Check for errors
	if tx.Error != nil {
		log.Println(tx.Error)
		return nil, tx.Error
	}

	// Retrieve the updated product record from the database
	updatedProduct := &Product{}
	if err := r.db.First(&updatedProduct, id).Error; err != nil {
		return nil, err
	}

	return updatedProduct, nil
}
func (r productRepositoryDB) Delete(pid int) error {
	// Use GORM to delete the product record with the specified ID
	tx := r.db.Delete(&Product{}, pid)

	// Check for errors
	if tx.Error != nil {
		return tx.Error
	}

	return nil
}
