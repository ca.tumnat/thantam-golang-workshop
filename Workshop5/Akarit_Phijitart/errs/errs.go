package errs

import "net/http"

type AppError struct {
	Code    int
	Massage string
}

func (e AppError) Error() string {
	return e.Massage
}

func NewNotfoundError(massage string) error {
	return AppError{
		Code:    http.StatusNotFound,
		Massage: massage,
	}
}

func NewUnExpectedError() error {
	return AppError{
		Code:    http.StatusInternalServerError,
		Massage: "Unexpected error",
	}
}
