package handler

import (
	"encoding/json"
	"fmt"
	"server/service"
	"time"

	"github.com/go-redis/redis"
	"github.com/gofiber/fiber/v2"
)

type customerHandlerRedis struct {
	custSrv     service.CustomerService
	redisClient *redis.Client
}

func NewCustomerHandlerRedis(custSrv service.CustomerService, redisClient *redis.Client) CustomerHandler {
	return customerHandlerRedis{custSrv, redisClient}
}

func (h customerHandlerRedis) GetCustomers(c *fiber.Ctx) error {

	key := "handler::GetCustomers"

	// Redis GET
	if responseJson, err := h.redisClient.Get(key).Result(); err == nil {
		fmt.Println("redis")
		c.Set("Content-Type", "application/json")
		return c.SendString(responseJson)
	}

	// Service
	customers, err := h.custSrv.GetCustomers()
	if err != nil {
		return err
	}

	response := fiber.Map{
		"status":   "ok",
		"products": customers,
	}

	// Redis SET
	if data, err := json.Marshal(response); err == nil {
		h.redisClient.Set(key, string(data), time.Second*10)
	}

	fmt.Println("database")
	return c.JSON(response)

}
