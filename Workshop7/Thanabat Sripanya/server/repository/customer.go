package repository

import (
	"fmt"
	"math/rand"
	"time"

	"gorm.io/gorm"
)

type Customer struct {
	CustomerID   int    `gorm:"column:id" json:"Customer_id" `
	CustomerName string `json:"Customer_name"`
	PhoneNumber  string `json:"Phone_number"`
	BirthDate    string `json:"Birth_date"`
}

type CustomerRepository interface {
	GetCustomers() ([]Customer, error)
}

func mockData(db *gorm.DB) error {
	var count int64
	db.Model(&Customer{}).Count(&count)
	if count > 0 {
		return nil
	}

	seed := rand.NewSource(time.Now().UnixNano())
	random := rand.New(seed)

	customers := []Customer{}
	for i := 0; i < 300; i++ {
		customers = append(customers, Customer{
			CustomerName: fmt.Sprintf("Customer %v", i+1),
			PhoneNumber:  fmt.Sprintf("%v", random.Intn(100000000)),
			BirthDate:    fmt.Sprintf(time.Now().String()),
		})
	}
	return db.Create(&customers).Error
}
